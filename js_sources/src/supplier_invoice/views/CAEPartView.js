import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { formatDate } from '../../date.js';
import { formatAmount } from '../../math.js';

/** Block with details about CAE part of the invoice.
 */
const CAEPartView = Mn.View.extend({
    template: require('./templates/CAEPartView.mustache'),
    initialize(){
        this.facade = Radio.channel('facade');
        let supplierInvoice = this.facade.request('get:model');
        this.payments = supplierInvoice.get('payments');
        this.supplierName = supplierInvoice.get('supplier_name');
    },
    /*** Prepare payment for rendering into template
     */
    paymentTemplateContext(payment){
        let context = _.clone(payment);
        context.amount = formatAmount(payment.amount);
        context.date = formatDate(payment.date);
        return context;
    },
    templateContext(){
        return {
            payments: this.payments.map(this.paymentTemplateContext),
            multiPayment: this.payments.length > 1,
            noPayment: this.payments.length < 1,
            supplierName: this.supplierName,
        };
    },
});
export default CAEPartView;
