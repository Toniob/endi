/*
 * Module name : PriceStudyResume
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

const template = require('./templates/PriceStudyResume.mustache');

const PriceStudyResume = Mn.View.extend({
    tagName: 'table',
    className: 'summary',
    template: template,
    modelEvents: {'sync': 'render'},
    templateContext(){
        return {
            ht_label: this.model.ht_label(),
            tva_labels: this.model.tva_labels(),
            ttc_label: this.model.ttc_label(),
        };
    }
});
export default PriceStudyResume
