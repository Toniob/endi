/*
 * Module name : ProductResume
 */
import Mn from 'backbone.marionette';


const ProductResume = Mn.View.extend({
    template: require('./templates/ProductResume.mustache'),
    modelEvents: {'sync': 'render'},
    templateContext(){
        return {
            supplier_ht_label: this.model.supplier_ht_label(),
            ht_label: this.model.ht_label(),
            total_ht_label: this.model.total_ht_label()
        };
    }
});
export default ProductResume
