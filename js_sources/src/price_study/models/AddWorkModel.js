/*
 * File Name :  AddWorkModel
 */
import Bb from 'backbone';
import Radio from 'backbone.radio';
import { formatAmount, round, sumArray } from '../../math.js';
import WorkItemCollection from './WorkItemCollection.js';
import ProductModel from './ProductModel.js';

const AddWorkModel = ProductModel.extend({
    /* props spécifique à ce modèle */
    props: [
        'title',
        'sale_product_work_id',
        "description",
        "type_",
    ],
    validation: {
        title: {required: true, msg: "Veuillez saisir un titre"},
        description: {required: true, msg: "Veuillez saisir un titre"}
    },
    defaults(){
        let config = Radio.channel('config');
        let defaults = config.request('get:options', 'defaults');
        return {
            margin_rate: defaults['margin_rate'],
            general_overhead: defaults['general_overhead'],
            tva_id: defaults['tva_id'],
            type_: 'price_study_work',
        };
    },
});
/*
 * On complète les 'props' du ProductModel avec celle du AddWorkModel
 */
AddWorkModel.prototype.props = AddWorkModel.prototype.props.concat(ProductModel.prototype.props);
_.extend(AddWorkModel.prototype.validation, ProductModel.prototype.validation);
export default AddWorkModel;
