import _ from 'underscore';
import Radio from 'backbone.radio';
import {ajax_call} from '../../../tools.js';
import {strToFloat} from '../../../math.js';
import OrderableCollection from "../../../base/models/OrderableCollection.js";
import TaskLineModel from './TaskLineModel.js';

const TaskLineCollection = OrderableCollection.extend({
    model: TaskLineModel,
    initialize: function(options) {
        TaskLineCollection.__super__.initialize.apply(this, options);
        this.on('saved', this.channelCall);
        this.on('destroyed', this.channelCall);
    },
    channelCall: function(){
        console.log("Calling changed:task");
        var channel = Radio.channel('facade');
        channel.trigger('changed:task');
    },
    ht: function(){
        var result = 0;
        this.each(function(model){
            result += model.ht();
        });
        return result;
    },
    tvaParts: function(){
        var result = {};
        this.each(function(model){
            var tva_amount = model.tva();
            var tva = model.get('tva');
            if (tva in result){
                tva_amount += result[tva];
            }
            result[tva] = tva_amount;
        });
        return result;
    },
    ttc: function(){
        var result = 0;
        this.each(function(model){
            result += model.ttc();
        });
        return result;
    },
    updateCurrentPercent(value){
        /*
         * Update the current_percent attribute of all items in the collection
         */
        this.each(
            function(model){
                if (strToFloat(value) != strToFloat(model.get('current_percent'))){
                    model.set('current_percent', value);
                }
            }
        );
    },
    validate: function(){
        return {};
    }
});
export default TaskLineCollection;
