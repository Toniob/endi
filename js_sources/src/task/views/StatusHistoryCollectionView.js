import Mn from 'backbone.marionette';
import { formatDate } from '../../date.js';

const StatusHistoryItemView = Mn.View.extend({
    tagName: 'div',
    className: 'row',
    template: require('./templates/StatusHistoryItemView.mustache'),
    templateContext(){
        return {
            date: formatDate(this.model.get('date'))
        };
    }
});

const StatusHistoryCollectionView = Mn.CollectionView.extend({
    tagName: 'div',
    childView: StatusHistoryItemView
});
export default StatusHistoryCollectionView;
