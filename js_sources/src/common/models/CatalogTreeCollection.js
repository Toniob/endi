/*
 * File Name : CatalogTreeCollection.js
 */
import Bb from 'backbone';
import CatalogTreeModel from './CatalogTreeModel.js';

const CatalogTreeCollection = Bb.Collection.extend({
    model: CatalogTreeModel,
    initialize(){
        CatalogTreeCollection.__super__.initialize.apply(this, arguments);
        this.current = null;
    },
    getSelected(){
        return this.current;
    },
    setSelected(model){
        this.current = model;
        this.trigger('change:selected');
    }
});
export default CatalogTreeCollection;
