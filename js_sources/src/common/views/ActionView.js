import $ from 'jquery';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ActionCollection from '../models/ActionCollection.js';
import ActionListView from './ActionListView.js';

var template = require("./templates/ActionView.mustache");

const ActionView = Mn.View.extend({
    regions: {
        container: "#more-actions",
    },
    ui: {
        statusButtons: '.btn-status',
    },
    events: {
        'click @ui.statusButtons': 'onStatusButtonClick'
    },
    modelEvents: {
        'change': 'render'
    },
    template: template,
    templateContext: function(){
        return {
            buttons: this.getOption('actions')['status'],
        }
    },
    onStatusButtonClick: function(event){
        let target = $(event.target);
        let status = target.data('status');
        let title = target.data('title');
        let label = target.data('label');
        let url = target.data('url');
        this.triggerMethod('status:change', status, title, label, url);
        var facade = Radio.channel('facade');
        facade.trigger('status:change', status, title, label, url);
    },
    onRender: function(){
        const action_collection = new ActionCollection(
            this.getOption('actions')['others']
        );
        this.showChildView(
            'container',
            new ActionListView({collection: action_collection})
        );
    }
});
export default ActionView;
