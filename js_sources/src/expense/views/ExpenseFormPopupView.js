import Mn from 'backbone.marionette';
import ModalBehavior from '../../base/behaviors/ModalBehavior.js';
import TelExpenseFormView from './TelExpenseFormView.js';
import RegularExpenseFormView from './RegularExpenseFormView.js';
import Radio from 'backbone.radio';
import BookMarkCollectionView from './BookMarkCollectionView.js';

const ExpenseFormPopupView = Mn.View.extend({
    behaviors: [ModalBehavior],
    template: require('./templates/ExpenseFormPopupView.mustache'),
    id: "expense-form-popup-modal",
    regions: {
        main: '#mainform-container',
        tel: '#telform-container',
        bookmark: '#bookmark-container',
    },
    ui: {
        main_tab: 'ul.nav-tabs li.main a',
        tel_tab: "ul.nav-tabs li.tel a",
        modalbody: 'main',
    },
    childViewEvents: {
        'bookmark:insert': 'onBookMarkInsert',
        'success:sync': 'onSuccessSync',
    },
    // Here we bind the child FormBehavior with our ModalBehavior
    // Like it's done in the ModalFormBehavior
    childViewTriggers: {
        'cancel:form': 'modal:close',
        'bookmark:delete': 'bookmark:delete',
    },
    modelEvents: {
        'set:bookmark': 'refreshForm',
    },
    initialize(){
        var facade = Radio.channel('facade');
        this.bookmarks = facade.request('get:bookmarks');
        this.add = this.getOption('add');
        this.tel = this.model.isTelType();
    },
    refresh(){
        this.triggerMethod('line:add', this.model.get('category'));
    },
    onSuccessSync(){
        if (this.add){
            var this_ = this;
            var modalbody = this.getUI('modalbody');

            modalbody.effect(
                'highlight',
                {color: 'rgba(0,0,0,0)'},
                800,
                this_.refresh.bind(this)
            );
            modalbody.addClass('action_feedback success');
        } else {
            this.triggerMethod('modal:close');
        }
    },
    onModalBeforeClose(){
        this.model.rollback();
    },
    refreshForm(){
        let area, view;
        let viewParams = {
            model: this.model,
            destCollection: this.getOption('destCollection'),
            title: this.getOption('title'),
            add: this.add,
        };

        if (this.shouldShowMainTab()) {
            this.showChildView('main', new RegularExpenseFormView(viewParams));
        }
        if (this.shouldShowTelTab()) {
            this.showChildView('tel', new TelExpenseFormView(viewParams));
        }

        let activeTab = this.tel ? 'tel_tab' : 'main_tab';
        this.getUI(activeTab).tab('show');
    },
    shouldShowMainTab() {
        return this.add || ! this.tel;
    },
    shouldShowTelTab() {
        return this.add || this.tel;
    },
    onBookMarkInsert(childView){
        this.model.loadBookMark(childView.model);
    },
    showBookMarks(){
        if (this.add){
            if (this.bookmarks.length > 0){
                var view = new BookMarkCollectionView({
                    collection: this.bookmarks
                });
                this.showChildView('bookmark', view);
            }
        }
    },
    templateContext(){
        /*
         * Form can be add form : show all tabs
         * Form can be tel form : show only the tel tab
         */
        return {
            title: this.getOption('title'),
            add: this.add,
            show_tel_tab: this.tel,
            show_tel: this.shouldShowTelTab(),
            show_bookmarks: this.add && this.bookmarks.length > 0,
            show_main: this.shouldShowMainTab(),
        };
    },
    onRender: function(){
        this.refreshForm();
        this.showBookMarks();
    },
});
export default ExpenseFormPopupView;
