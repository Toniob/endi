# -*- coding:utf-8 -*-
import logging
import datetime

logger = logging.getLogger(__name__)


def parse_date(value, default=None, format_="%Y-%m-%d"):
    """
    Get a date object from a string

    :param str value: The string to parse
    :param str default: The default value to return
    :param str format_: The format of the str date
    :returns: a datetime.date object
    """
    try:
        result = datetime.datetime.strptime(value, format_).date()
    except ValueError as err:
        logger.debug("{} is not a date".format(value))
        if default is not None:
            result = default
        else:
            raise err
    return result


def get_current_year():
    return datetime.date.today().year
