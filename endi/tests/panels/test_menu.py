# -*- coding: utf-8 -*-
from endi.tests.tools import (
    Dummy,
    DummyRoute,
)

from endi.panels.menu import (
    get_current_company,
    get_companies,
)


def get_context(cid=200):
    context = Dummy()
    context.get_company_id = lambda: cid
    return context


def test_get_cid(
    get_csrf_request_with_db, dbsession, mk_user, mk_company
):
    user = mk_user(lastname="test", firstname="test")
    user2 = mk_user(lastname="test2", firstname="test2")
    company = mk_company("a", "b", "test", employee=user)
    company2 = mk_company("c", "d", "test", employee=user2)
    dbsession.merge(user)
    dbsession.flush()

    request = get_csrf_request_with_db(user=user)
    assert get_current_company(request).id == company.id
    assert request.current_company == company

    request.context = company2
    # reset du cache
    request.current_company = None
    assert get_current_company(request).id == company.id
    # reset du cache
    request.current_company = None
    assert get_current_company(request, submenu=True).id == company2.id


def test_get_companies(
    config, pyramid_request, user, company, company2, dbsession
):
    config.testing_securitypolicy(userid="test", permissive=False)
    pyramid_request.user = user
    pyramid_request.context = get_context()
    user.companies = [company]
    dbsession.merge(user)
    dbsession.flush()
    assert get_companies(pyramid_request, None) == \
        pyramid_request.user.active_companies
    config.testing_securitypolicy(userid="test", permissive=True)
    assert get_companies(pyramid_request, company2) == \
        [company, company2]


def test_item(pyramid_request, config):
    from endi.panels.menu import Item
    config.add_route("/a", "/a")
    config.add_route("/b", "/b")
    config.add_route("/b", "/b")
    config.add_route("/c", "/c")
    config.add_route("/a/{id}", "/a/{id}")
    config.add_route("/z/{id}", "/z/{id}")

    i1 = Item(label="", icon="", href="/a")
    i2 = Item(label="", icon="", href="/b")
    i3 = Item(label="", icon="", href="/c", routes_prefixes=['/a'])

    pyramid_request.matched_route = DummyRoute(name="/a")

    assert i1.selected(pyramid_request)
    assert not i2.selected(pyramid_request)
    assert i3.selected(pyramid_request)

    pyramid_request.matched_route = DummyRoute(name="/a/{id}", id=12)
    pyramid_request.matchdict = {'id': 12}

    assert not i1.selected(pyramid_request)
    assert not i2.selected(pyramid_request)
    assert i3.selected(pyramid_request)

    pyramid_request.matched_route = DummyRoute(name="/z/{id}", id=12)
    pyramid_request.matchdict = {'id': 12}

    assert not i1.selected(pyramid_request)
    assert not i2.selected(pyramid_request)
    assert not i3.selected(pyramid_request)


def test_menuitem():
    from endi.utils.menu import MenuItem
    mi = MenuItem("z", 'test_list', 'fa-pen', label="é")
    assert mi.get_label() == "é"
    assert mi.get_title() == "é"

    mi = MenuItem("z", 'test_list', 'fa-pen', label="<em>é</em>")
    assert mi.get_label() == "<em>é</em>"
    assert mi.get_title() == "é"

    mi = MenuItem("z", 'test_list', 'fa-pen', label="<em>é</em>", title='fu')
    assert mi.get_label() == "<em>é</em>"
    assert mi.get_title() == "fu"


def test_menu():
    from endi.panels.menu import Menu
    a = Menu()
    assert not(a)
    a.add('test')
    assert bool(a)
