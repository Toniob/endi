

def test_menuitem_deferreds():
    from endi.utils.menu import MenuItem

    def deferred_label(self, kw):
        return 'mylab' + kw['x']

    def deferred_title(self, kw):
        return 'mytitle'

    mi = MenuItem(
        "z",
        'test_list',
        'fa-pen',
        label=deferred_label,
        title=deferred_title,
    )
    assert mi.get_label(x='el') == "mylabel"
    assert mi.get_title(x='el') == "mytitle"
