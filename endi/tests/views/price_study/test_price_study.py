# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound


def test_price_study_add_view(
    config, get_csrf_request_with_db, user, project, company
):
    from endi.views.price_study.price_study import PriceStudyAddView
    from endi.views.price_study.routes import PRICE_STUDY_ROUTE
    from endi.models.price_study.price_study import PriceStudy

    config.add_route(PRICE_STUDY_ROUTE, PRICE_STUDY_ROUTE)
    project.company_id = company.id

    request = get_csrf_request_with_db(
        post={'name': 'Name', "notes": "Notes", 'submit': 'submit'},
        user=user,
    )
    request.context = project
    view = PriceStudyAddView(project, request)
    result = view.__call__()
    assert isinstance(result, HTTPFound)
    p = PriceStudy.query().first()
    assert p.name == 'Name'
    assert p.notes == 'Notes'
    assert p.owner_id == user.id
    assert p.company_id == company.id
    assert p.project_id == project.id

    request = get_csrf_request_with_db(
        post={'name': '', "notes": "Notes", 'submit': 'submit'},
        user=user,
    )
    view = PriceStudyAddView(project, request)
    result = view.__call__()
    assert isinstance(result, dict)
