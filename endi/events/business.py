import logging

from sqlalchemy.event import listen

from endi.models.training import BusinessBPFData
from endi.models.task.invoice import Invoice
from endi.models.project.business import Business
from endi.models.services.business_status import BusinessStatusService
from endi.utils.datetimes import get_current_year



logger = logging.getLogger(__name__)


def update_business_bpf_indicator(mapper, connection, target):
    if isinstance(target, Business):
        business = target
    elif isinstance(target, BusinessBPFData) or isinstance(target, Invoice):
        business = Business.query().filter_by(id=target.business_id).first()
    if business.business_type.bpf_related:
        logger.info("+ Updating Business BPF indicator")
        BusinessStatusService.update_bpf_indicator(business)

from sqlalchemy.orm import Session


def dispatch_event(session, flush_context, instances):
    # We prefer before_flush event rather than more fine-grained after_* events
    # because they allow to do db stuff in callback.
    # https://docs.sqlalchemy.org/en/13/orm/events.html#sqlalchemy.orm.events.MapperEvents.before_insert
    for obj in session.dirty:
        if isinstance(obj, (Business, Invoice, BusinessBPFData)):
            update_business_bpf_indicator(None, None, obj)


def includeme(config):
    listen(Session, "before_flush", dispatch_event, once=True)
