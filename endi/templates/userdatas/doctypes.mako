<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_filetable" />
<%block name="mainblock">
<div class='alert alert-info'>
<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
Cochez ici les documents sociaux que l’entrepreneur a déjà transmis
</div>
<div class='doc_list'>
${form|n}
</div>
</%block>
