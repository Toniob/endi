<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name="headtitle">
${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>

<%block name='actionmenucontent'>
<% expense = request.context %>
<div class='layout flex main_actions'>
    <div id="js_actions" class="js_actions"></div>
    <div role="group">
        <button class='btn icon only' onclick="window.print()" title="Imprimer" aria-label="Imprimer">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#print"></use></svg>
        </button>
        <a class='btn icon only' href='${request.route_path("/expenses/{id}.xlsx", id=expense.id)}' title='Export au format Excel (xls)' aria-label='Export au format Excel (xls)'>
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg>
        </a>
    </div>
</div>
</%block>

<%block name="beforecontent">
<% expense = request.context %>
<div>
    <div class='expense_header layout flex two_cols'>
        <div>
            <h3 class='hidden-print'>
                ${request.layout_manager.render_panel('status_title', context=request.context)}
            </h3>
            <ul class="content_vertical_padding">
                <li class='hidden-print'>
                    ${api.format_expense_status(expense)}
                </li>
                % if request.has_permission('admin_treasury'):
                    <li>
                    	Numéro de pièce :
                        % if expense.status == 'valid':
                            <strong>${ expense.id }</strong>
                        % else:
                            <strong>Ce document n’a pas été validé</strong>
                        % endif
                    </li>
                    <li>
                        % if expense.purchase_exported and expense.expense_exported:
                            Ce document a déjà été exporté vers le
                            logiciel de comptabilité
                        % elif expense.purchase_exported:
                            Les achats déclarés dans ce document ont déjà été
                            exportés vers le logiciel de comptabilité
                        % elif expense.expense_exported:
                            Les frais déclarés dans ce document ont déjà été
                            exportés vers le logiciel de comptabilité
                        %else:
                            Ce document n'a pas encore été exporté vers le logiciel de comptabilité
                        % endif
                    </li>
                    <li>
                    	Porteur de projet : ${api.format_account(expense.user)}</li>
                % endif
                % if expense.payments:
                    <li>
                        Paiement(s) recu(s):
                        <ul>
                            % for payment in expense.payments:
                                <% url = request.route_path('expense_payment', id=payment.id) %>
                                <li>
                                <a href="${url}">
                                    Par ${api.format_account(payment.user)}&nbsp;: 
                                    ${api.format_amount(payment.amount)}&nbsp;€
                                    le ${api.format_date(payment.date)}
                                    % if payment.waiver:
                                        <br />(par abandon de créances)
                                    % else:
                                        % if payment.bank:
                                            <br />(${api.format_paymentmode(payment.mode)}&nbsp;${payment.bank.label})
                                        % else:
                                            <br />(${api.format_paymentmode(payment.mode)})
                                        % endif
                                    % endif
                                </a>
                                </li>
                            % endfor
                        </ul>
                    </li>
                % endif
            </ul>
            <div class="hidden-print separate_top content_vertical_padding">
                <h3>Justificatifs</h3>
                <div class="content_vertical_padding">
                    % if expense.justified:
                        <span class='icon status success'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg></span>
                        Justificatifs reçus
                    % endif
                </div>
                ${format_filelist(expense)}
                % if not expense.children:
                    <p><small><em>Aucun justificatif n’a été déposé</em></small></p>
                % endif
                <a href="${request.route_path('/expenses/{id}/addfile', id=expense.id)}" class="btn btn-primary" title="Déposer un justificatif de dépense">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>Ajouter un justificatif
                </a>
            </div>
        </div>
        <div class="hidden-print">
            <h4>Historique des communications</h4>
            % for com in communication_history:
                % if com.content.strip():
                    <blockquote>
                        <p>${format_text(com.content)}</p>
                        <footer>${api.format_account(com.user)} le ${api.format_date(com.date)}</footer>
                    </blockquote>
                % endif
            % endfor
        </div>
    </div>
</div>
</%block>

<%block name='content'>
<div id="js-main-area"></div>
</%block>

<%block name='footerjs'>
var AppOption = {};
AppOption['context_url'] = "${context_url}";
AppOption['form_config_url'] = "${form_config_url}";
AppOption['csrf_token'] = "${get_csrf_token()}";
% if request.has_permission("edit.expensesheet"):
    AppOption['edit'] = true;
% else:
    AppOption['edit'] = false;
% endif
</%block>
