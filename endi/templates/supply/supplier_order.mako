<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_filelist" />
<%block name="headtitle">
    ${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>
<%block name='actionmenucontent'>
<div class="layout flex main_actions">
    <div id="js_actions" class="js_actions"></div>
</div>
</%block>
<%block name="beforecontent">
    <% supplier_order = request.context %>
    <div>
        <div class='layout flex two_cols hidden-print'>
            <div>
                <h3>
                    ${request.layout_manager.render_panel('status_title', context=request.context)}
                </h3>
                <ul class="content_vertical_padding">
                    % if supplier_order.status == 'valid':
                    <li>
                        Facture fournisseur :
                        % if supplier_order.supplier_invoice:
                            <% invoice = supplier_order.supplier_invoice %>
                            <a href="${request.route_path('/suppliers_invoices/{id}', id=invoice.id)}">
                                ${invoice.name} de ${api.format_amount(invoice.total)}&nbsp;€
                            </a>
                        % else:
                            Aucune pour l'instant
                        % endif
                    </li>
                    % endif
                </ul>

                <div class="separate_top content_vertical_padding">
                    <h3>
                        Justificatifs
                    </h3>
                    ${format_filelist(supplier_order)}
                    % if not supplier_order.children:
				        <p><small><em>
					        Aucun justificatif n’a été déposé
				        </em></small></p>
                    % endif
                    <a
                        href="${request.route_path('/suppliers_orders/{id}/addfile', id=supplier_order.id)}"
                        class="btn btn-primary">
                        ${api.icon('plus')}
                        Ajouter un justificatif
                    </a>
                </div>
            </div>
            <div>
                % if supplier_order.validation_status_history:
                    <h4>Historique des Communications Entrepreneurs-CAE</h4>
                % endif
                % for entry in supplier_order.validation_status_history:
                    % if entry.comment.strip():
                        <blockquote>
                            <p>
                                <span class="icon">${api.icon(api.status_icon(entry))}</span>
                                ${format_text(entry.comment)}
                            </p>
                            <footer>
                                ${api.format_account(entry.user)} le ${api.format_date(entry.datetime)}
                            </footer>
                        </blockquote>
                    % endif
                % endfor
            </div>
        </div>
    </div>
</%block>
<%block name='content'>
    <div id="js-main-area"></div>
</%block>
<%block name='footerjs'>
    var AppOption = {};
    AppOption['context_url'] = "${context_url}";
    AppOption['form_config_url'] = "${form_config_url}"
    % if request.has_permission("edit.supplier_order"):
        AppOption['edit'] = true;
    % else:
        AppOption['edit'] = false;
    % endif
</%block>
