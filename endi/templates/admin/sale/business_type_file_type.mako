<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%block name='afteradminmenu'>
</%block>
<%block name='content'>
<div>
	<form method='POST'
		class="deform  deform" accept-charset="utf-8"
		enctype="multipart/form-data">
		<input type='hidden' name='__start__' value='items:sequence' />
		% for business_type in business_types:
		<h2>${business_type.label}</h2>
		<div class="table_container separate_bottom">
			<table class='top_align_table'>
				<thead>
					<tr>
						<th scope="col" class="col_text">Types de fichier</th>
						% if business_type.name != 'default':
							<th scope="col" class="col_text">Affaire</th>
						% endif
						<th scope="col" class="col_text">Devis</th>
						<th scope="col" class="col_text">Factures</th>
						<th scope="col" class="col_text">Avoirs</th>
					</tr>
				</thead>
				<tbody>
				% for file_type in file_types:
					<% file_type_items = items.get(file_type.id, {}) %>
					<tr>
						<th scope="row" class="col_text">${file_type.label}</th>
						<% btype_items = file_type_items.get(business_type.id, {}) %>
						% for doctype in ('business', 'estimation', 'invoice', 'cancelinvoice'):
							% if business_type.name == 'default' and doctype == 'business':
							<% continue %>
							% endif
						<% requirement_type = btype_items.get(doctype, {}).get('requirement_type', -1) %>
						<% validation = btype_items.get(doctype, {}).get('validation') %>
						<% tag_id = "requirement_type_%s_%s_%s" % (file_type.id, business_type.id, doctype) %>
						<td class="col_text">
							<input type='hidden' name='__start__' value='item:mapping' />
							<input type='hidden' name='file_type_id' value='${file_type.id}' />
							<input type='hidden' name='business_type_id' value='${business_type.id}'/>
							<input type='hidden' name='doctype' value='${doctype}' />
							<input type='hidden' name='__start__' value='requirement_type:rename'>
							<label for='${tag_id}'>Ce type de fichier&nbsp;: </label>
							<div class='radio'>
							<label>
								<input type='radio' name='${tag_id}' value=''
								% if requirement_type == -1:
								checked
								% endif
								/>N’est pas proposé dans le formulaire de dépot de fichier
							</label>
							</div>
							% for option, label in (\
								('optionnal', u'Est proposé dans le formulaire de dépot de fichier'), \
								('recommended', u'Est recommandé'), \
								('mandatory', u'Est requis systématiquement pour la validation'), \
								('business_mandatory', u"Est globalement requis dans le/la {0} pour la validation".format(business_type.label)), \
								('project_mandatory', u"Est globalement requis dans le dossier pour la validation")):
								% if doctype == 'business' and option == 'mandatory':
								<% continue %>
								% endif

								<div class='radio'>
								<label>
									<input type='radio' name='${tag_id}' value='${option}'
									% if requirement_type == option:
									checked
									% endif
									/>${label}
								</label>
								</div>
							% endfor
							<input type='hidden' name='__end__' value='requirement_type:rename'>
							<hr />
							<div class="checkbox">
							<label>
							  <input type="checkbox" name="validation"
							  % if validation:
							  checked
							  % endif
							  >Ce type de fichier exige une validation par l’équipe d’appui&nbsp;?
							</label>
							</div>
						<input type='hidden' name='__end__' value='item:mapping' />
						</td>
					% endfor
					</tr>
					% endfor
				</tbody>
			</table>
		</div>
		% endfor
		<input type='hidden' name='__end__' value='items:sequence' />
		<div class='form-actions'>
		   <button id="deformsubmit" class="btn btn-primary" value="submit" type="submit" name="submit"> Enregistrer </button>
		</div>
	</form>
</div>
</%block>
